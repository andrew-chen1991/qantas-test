import { request, db } from "./mocks";

/* 1.1
  RequestReturnObject:
    data: Array,
    status: Number,
    statusText: String,
    content-type: String,
    set-cookie: String,
    url: String,
 */

const RequestReturnObject = {
  data: [], // array
  status: 0, // number
  statusText: "", // string
  "content-type": "", // string
  "set-cookie": "", // string
  url: "" // string
};

// 1.2
const getAndStoreResults = urls => urls.map(url => db.save(request.get(url)));

// 1.3
const getStoreAndMaybePrint = urls =>
  urls.map(url => {
    const result = request.get(url);
    db.save(result);

    if (url.indexOf("http://test.com/") === 0) {
      console.log(result);
    }
  });

// 1.4
const findCookieSetters = () =>
  db.find("headers.set-cookie", /([^s]*)/).map(obj => obj.url);
