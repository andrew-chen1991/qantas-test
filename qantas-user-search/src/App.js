import React, { Component } from 'react';
import { TextField, Button } from '@material-ui/core';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';
import { withValidation } from './withValidation';

class App extends Component {
  state = {
    input: '',
    result: '',
  };

  updateInput = e =>
    this.setState({
      input: e.target.value,
    });

  search = () =>
    axios
      .get(`http://localhost:3001/users?name=${this.state.input}`)
      .then(response => {
        // handle success
        this.setState({ result: response.data });
      })
      .catch(error => {
        // handle error
        console.log(error);
      });

  // HOC for validation
  ButtonWithValidation = withValidation(Button);

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Qantas User Search</h1>
        </header>
        <section>
          <div>User: </div>
          <TextField value={this.state.input} onChange={this.updateInput} />
          <this.ButtonWithValidation
            value={this.state.input}
            onClick={this.search}
            variant="outlined"
          >
            Search
          </this.ButtonWithValidation>
        </section>
        <section>
          {this.state.result && JSON.stringify(this.state.result)}
        </section>
      </div>
    );
  }
}

export default App;
