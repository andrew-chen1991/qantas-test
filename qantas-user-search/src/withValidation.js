import React, { Component } from 'react';

export const withValidation = WrappedComponent => {
  return class extends Component {
    render() {
      return (
        <div>
          <WrappedComponent
            {...this.props}
            onClick={this.props.value && this.props.onClick}
          />
          {!this.props.value && <div>Error! No input!</div>}
        </div>
      );
    }
  };
};
